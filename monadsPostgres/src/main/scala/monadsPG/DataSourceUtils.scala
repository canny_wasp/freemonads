package monadsPG

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import monadsFlyway.FlywayUtils
import pureconfig.generic.auto._

object DataSourceUtils {

  private final var dataSource: Option[HikariDataSource] = None

  final case class HickariConfig(dbConfig: DbConfig)
  final case class DbConfig(container: Boolean,
                                    connectPoolSize: Int,
                                    transactionPoolSize: Int,
                                    driverClassName: String,
                                    url: String,
                                    dbName: String,
                                    username: String,
                                    password: String)

  final def hikariDataSource(): HikariDataSource = dataSource match {
    case Some(ds) => ds
    case None =>
      val ds = initNewDataSource()
      FlywayUtils.migrateDatabase(ds)
      dataSource = Some(ds)
      ds
  }

  @inline
  private def initNewDataSource(): HikariDataSource = {
    implicit val dbConf: DbConfig = pureconfig.loadConfigOrThrow[HickariConfig].dbConfig

    val hikariConfig =
      if (dbConf.container) {
        val container = ContainerUtils.container
        new HikariConfig() {
          setDriverClassName(container.getDriverClassName)
          setJdbcUrl(container.getJdbcUrl)
          setUsername(container.getUsername)
          setPassword(container.getPassword)
        }
      } else {
        new HikariConfig() {
          setDriverClassName(dbConf.driverClassName)
          setJdbcUrl(dbConf.url)
          setUsername(dbConf.username)
          setPassword(dbConf.password)
        }
      }
    new HikariDataSource(hikariConfig)
  }
}
