package monadsPG

import cats.effect.{IO, Resource}
import doobie.DataSourceTransactor

object DoobieTypes {
  final type ResTransactor[M[_]] = Resource[IO, DataSourceTransactor[IO]]
}
