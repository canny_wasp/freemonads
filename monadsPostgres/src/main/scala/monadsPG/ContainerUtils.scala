package monadsPG

import monadsPG.DataSourceUtils.DbConfig
import org.testcontainers.containers.PostgreSQLContainer

object ContainerUtils {
  final class ScalaPostgresSQLContainer(image: String)
      extends PostgreSQLContainer[ScalaPostgresSQLContainer](image)

  private var container: Option[ScalaPostgresSQLContainer] = None

  def container()
               (implicit config: DbConfig): ScalaPostgresSQLContainer = container match {
    case Some(cn) => cn
    case None => initNewContainer()
  }

  @inline private def initNewContainer()
                                      (implicit config: DbConfig): ScalaPostgresSQLContainer = {
    val container = new ScalaPostgresSQLContainer("postgres:11.4")
      .withDatabaseName(config.dbName)
      .withUsername(config.username)
      .withPassword(config.password)
    container.start()
    this.container = Some(container)
    container
  }
}

