
lazy val _scalaVersion = "2.12.7"
lazy val _sbtVersion = "1.2.8"

ThisBuild / organization := "com.mayur"
ThisBuild / version      := "0.0.2"
ThisBuild / scalaVersion := _scalaVersion
ThisBuild / scalacOptions ++= Seq (
  "-encoding", "utf8",
  "-Ypartial-unification",
  "-Xfatal-warnings",
  "-Xverify",
  "-deprecation",
  "-unchecked",
  "-target:jvm-1.8",
  "-language:implicitConversions",
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps"
)

lazy val doobieVersion = "0.7.0"
lazy val http4sVersion = "0.20.0"
lazy val slf4jVersion = "1.7.26"
lazy val catsVersion = "1.6.0"
lazy val catsEffectVersion = "1.2.0"
lazy val scalaTestVersion = "3.0.8"
lazy val circeVersion = "0.11.1"
lazy val pureConfigVersion = "0.11.1"

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

lazy val monadsFlyway = (project in file("monadsFlyway"))
  .settings(
    name := "monadsFlyway",
    scalaVersion := _scalaVersion,
    sbtVersion := _sbtVersion,
    libraryDependencies ++= List(
      "com.github.pureconfig" % "pureconfig_2.12" % pureConfigVersion,
      "org.flywaydb" % "flyway-core" % "5.0.3",
    )
  ).disablePlugins(AssemblyPlugin)

lazy val monadsPostgres = (project in file("monadsPostgres"))
  .settings(
    name := "monadsPostgres",
    scalaVersion := _scalaVersion,
    sbtVersion := _sbtVersion,
    libraryDependencies ++= List(
      "org.testcontainers" % "postgresql" % "1.11.2",

      "org.tpolecat" % "doobie-core_2.12"      % doobieVersion,
      "org.tpolecat" % "doobie-hikari_2.12"    % doobieVersion,
      "org.tpolecat" % "doobie-postgres_2.12"  % doobieVersion,
      "org.tpolecat" % "doobie-specs2_2.12"    % doobieVersion,
    )
  ).dependsOn(
    monadsFlyway % "compile->compile"
  ).disablePlugins(AssemblyPlugin)

lazy val monadsWeb = (project in file("monadsWebAsync"))
  .settings(
    name := "monadsWebAsync",
    scalaVersion := _scalaVersion,
    sbtVersion := _sbtVersion,
    mainClass in Compile := Some("monadsWebAsync.MonadsWeb"),
    libraryDependencies ++= List(
      "org.http4s" % "http4s-dsl_2.12"          % http4sVersion,
      "org.http4s" % "http4s-blaze-server_2.12" % http4sVersion,
      "org.http4s" % "http4s-blaze-client_2.12" % http4sVersion,
      "org.http4s" % "http4s-circe_2.12"        % http4sVersion,

      "io.circe" % "circe-generic_2.12" % circeVersion,
      "io.circe" % "circe-literal_2.12" % circeVersion,

      "org.scalactic" % "scalactic_2.12" % scalaTestVersion,
      "org.scalatest" % "scalatest_2.12" % scalaTestVersion % "test",

      "com.typesafe.scala-logging" % "scala-logging_2.12" % "3.9.2",
      "ch.qos.logback" % "logback-classic" % "1.2.3"
    ),
    libraryDependencies += "org.influxdb" % "influxdb-java" % "2.14",
    assemblyJarName in assembly := "monads.jar"
  )
  .dependsOn(
    monadsPostgres % "compile->compile"
  )

lazy val root = (project in file("."))
    .settings(
      name := "monads",
      scalaVersion := _scalaVersion,
      sbtVersion := _sbtVersion,
    ).aggregate(
      monadsWeb
    )
