package service

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.FlatSpec
import monadsWebAsync.dao.UserType
import monadsWebAsync.utils.DI._

import scala.collection.immutable

class UserTypeServIOTest extends FlatSpec with LazyLogging {

  it should "find all types" in {
    val types: immutable.Seq[UserType] = uTypeService.findAll().unsafeRunSync()

    logger.info(s"All types :: $types")
    assert(types.nonEmpty)
    assert(types.map(_.name) contains "PERSON")
    assert(types.map(_.name) contains "COMPANY")
  }

}
