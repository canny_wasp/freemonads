package service

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfter, FlatSpec}
import monadsWebAsync.utils.DI._

class UserServiceIOTest extends FlatSpec with BeforeAndAfter with LazyLogging {

  before {
    logger.info("Before initialization")
  }

  it should "Create user with billing" in {

    val userWithBilling = (
      for {
        types <- uTypeService.findAll()
        withBilling <- userService.createWithBilling(types.head.id.get)
      } yield withBilling
    ).unsafeRunSync()

    logger.info(s"With billing :: $userWithBilling")
    assert(userWithBilling != null)
  }
}
