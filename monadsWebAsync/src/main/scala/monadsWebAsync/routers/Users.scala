package monadsWebAsync.routers

import cats.effect.IO
import io.circe.generic.auto._
import io.circe.syntax._
import monadsWebAsync.dao.User
import org.http4s.{EntityDecoder, HttpRoutes}
import org.http4s.circe.jsonOf
import org.http4s.dsl.io._
import org.http4s.circe._
import monadsWebAsync.service.UserService


object Users {
  implicit val decoder: EntityDecoder[IO, User] = jsonOf[IO, User]
  final def userController()
                          (implicit userService: UserService[IO]): HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / userType =>
      userService
        .create(userType.toShort)
        .flatMap(res => Ok(res.asJson))
        .handleErrorWith(err => InternalServerError(s"Cannot save user :: ${err.getMessage}"))
    case GET -> Root / userType / "withBilling" =>
      userService
        .createWithBilling(userType.toShort)
        .flatMap(res => Ok(res.asJson))
        .handleErrorWith(err => InternalServerError(s"Cannot save user :: ${err.getMessage}"))
  }
}
