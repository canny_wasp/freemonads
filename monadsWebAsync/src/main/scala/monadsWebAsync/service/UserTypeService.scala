package monadsWebAsync.service

import cats.effect.IO
import com.typesafe.scalalogging.LazyLogging
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import monadsWebAsync.dao.UserType
import monadsWebAsync.repository.UserTypeRepo
import monadsPG.DoobieTypes.ResTransactor

trait UserTypeAlg[M[_]] {
  def findAll(): M[List[UserType]]
}


final class UserTypeServiceIO()
                             (implicit val rxa: ResTransactor[IO],
                              implicit val userRepo: UserTypeRepo[ConnectionIO]
                             ) extends UserTypeAlg[IO] with LazyLogging {

  logger.info("Initialized")

  override def findAll(): IO[List[UserType]] = rxa.use { xa => userRepo.findAll().transact(xa) }
}