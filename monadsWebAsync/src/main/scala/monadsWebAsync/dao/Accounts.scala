package monadsWebAsync.dao

import java.util.UUID

final case class User(id: Option[Long] = None,
                      guid: String = UUID.randomUUID().toString,
                      uType: Short)


final case class Billing(id: Option[Long], userId: Long, amount: Long, active: Boolean)


final case class UserType(id: Option[Short],
                          name: String,
                          caption: String)
