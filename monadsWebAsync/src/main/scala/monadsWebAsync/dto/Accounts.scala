package monadsWebAsync.dto

import monadsWebAsync.dao._

final case class UserWithBilling(id: Long, guid: String, uType: Short, billingId: Long)
object UserWithBilling {
  def apply(t: (User, Billing)): UserWithBilling =
    new UserWithBilling(t._1.id.get, t._1.guid, t._1.uType, t._2.id.get)
}
