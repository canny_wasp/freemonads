package monadsWebAsync.utils

import cats.effect.{ContextShift, IO, Timer}
import com.zaxxer.hikari.HikariDataSource
import doobie.free.connection.ConnectionIO
import monadsWebAsync.repository._
import monadsWebAsync.routers.Health._
import monadsWebAsync.routers.UserTypes.userTypeController
import monadsWebAsync.routers.Users.userController
import monadsWebAsync.service._
import monadsWebAsync.utils.FreeMonadsConfig._
import monadsPG.DoobieTypes.ResTransactor
import monadsPG.DoobieUtils.resTransactor
import monadsPG.DataSourceUtils.{HickariConfig, hikariDataSource}
import monadsWebAsync.utils.InfluxUtils._
import org.http4s.HttpApp
import org.http4s.implicits._
import org.http4s.server.Router
import org.influxdb.InfluxDB
import pureconfig._
import pureconfig.generic.auto._

import scala.concurrent.ExecutionContext.global

object DI {

  implicit val webConfig: FreeMonadsConfig = loadConfigOrThrow[FreeMonadsConfig]
  implicit val hickariConfig: HickariConfig = loadConfigOrThrow[HickariConfig]

  implicit lazy val influx: Option[InfluxDB] = initInflux()
  implicit lazy val dataSource: HikariDataSource = hikariDataSource()

  implicit lazy val cs: ContextShift[IO] = IO.contextShift(global)
  implicit lazy val timer: Timer[IO] = IO.timer(global)
  implicit lazy val monitoringService: MonitoringAlg = new MonitoringService()
  /**
    * 3) Some types must be processed at try-catch-finally wrapper,
    * known as finalizer bracket, but, for our luck, there are ready
    * functional resource implementation, let's imagine it via
    * such simple abstraction:
    *
    * @ResTransactor[M[_]] =>
    *   @ResTransactor[IO] =>
    *   @ResourceT[IO,DataSourceTransactor[IO]] =>
    *   @Resource[IO[DataSourceTransactor[IO]]] =>
    *   @Resource[()=>DataSourceTransactor[()=>T]]]
    */
  implicit lazy val rtx: ResTransactor[IO] = resTransactor(dataSource)

  implicit lazy val userRepo: UserTypeRepo[ConnectionIO] = new UserTypeRepoIO()
  implicit lazy val userRepositoryIO: UserRepo[ConnectionIO] = new UserRepositoryIO()
  implicit lazy val billingRepo: BillingRepoAlg[ConnectionIO] = new BillingRepositoryIO()

  implicit lazy val userService: UserService[IO] = new UserServiceIO()
  implicit lazy val uTypeService: UserTypeAlg[IO] = new UserTypeServiceIO()

  val authService: AuthService[IO] = new AuthServiceIO()

  /**
    * 2) All values aka @Beans must be initialized lazy at least once,
    * after first initialization it will be memoize and reused by signature
    * of base types or traits
    */
  def httpApp(): HttpApp[IO] = {
    Router(
      "/health" -> healthController,
      "/user" -> userController(),
      "/userType" -> authService.authMiddleware(userTypeController())
    ).orNotFound
  }
}
