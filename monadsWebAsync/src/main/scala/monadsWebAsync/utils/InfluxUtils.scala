package monadsWebAsync.utils

import java.lang.System.currentTimeMillis

import org.influxdb.dto.{Point, Query}
import org.influxdb.{BatchOptions, InfluxDB, InfluxDBFactory}
import java.util.concurrent.TimeUnit
import monadsWebAsync.utils.FreeMonadsConfig._

object InfluxUtils {
  final type MaybeInfluxDB = Option[InfluxDB]

  final def initInflux()(implicit config: FreeMonadsConfig): MaybeInfluxDB = {
    val influxConf = config.influx

    if ( ! influxConf.enabled) return None

    val influxDB: InfluxDB = InfluxDBFactory.connect(influxConf.url, influxConf.name, influxConf.password)
    influxDB.query(new Query(s"CREATE DATABASE ${influxConf.dbName}", influxConf.dbName))
    influxDB.setDatabase(influxConf.dbName)
    val rpName = s"${influxConf.dbName}RetentionPolicy"
    influxDB.query(new Query(
      s"""
         CREATE RETENTION POLICY $rpName
         ON ${influxConf.dbName}
         DURATION 30h
         REPLICATION 1
         SHARD DURATION 3m DEFAULT
       """.stripMargin, influxConf.dbName))
    influxDB.setRetentionPolicy(rpName)
    val batchOptions: BatchOptions =
      BatchOptions.DEFAULTS.actions(2000).flushDuration(100)
    influxDB.enableBatch(batchOptions)
    influxDB.enableGzip()

    influxDB.write(
      Point.measurement("cpu")
        .time(currentTimeMillis, TimeUnit.MILLISECONDS)
        .addField("idle", 90L)
        .addField("user", 9L)
        .addField("system", 1L)
        .build)

    influxDB.write(Point
      .measurement("disk")
      .time(currentTimeMillis, TimeUnit.MILLISECONDS)
      .addField("used", 80L)
      .addField("free", 1L)
      .build)
    Some(influxDB)
  }

}
